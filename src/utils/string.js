/**
 * 小驼峰转中划线方法
 * @param str {string} 小驼峰字符串
 * @returns {string} 返回中划线变量字符串
 * @example
 * humpToMidline('humpToMidline') -> 'hump-to-midline'
 */
export function humpToMidline(str) {
  const hyphenateRE = /\B([A-Z])/g
  return str.replace(hyphenateRE, '-$1').toLowerCase()
}
