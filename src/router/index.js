/**
 * @file 路由文件
 * @author Jenson Miao 2022/4/15
 */
import { createRouter, createWebHashHistory } from 'vue-router'

const modules = import.meta.globEager('./modules/**/*.ts')
const routeModuleList = []

Object.keys(modules).forEach((key) => {
  const mod = modules[key].default || {}
  const modList = Array.isArray(mod) ? [...mod] : [mod]
  routeModuleList.push(...modList)
})

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/views/home/index.vue'),
    meta: {
      layout: 'default',
      activePath: '/home',
    },
  },
  ...routeModuleList,
  {
    path: '/:path(.*)*',
    name: 'PageNotFound',
    component: () => import('@/views/errorPage/404.vue'),
  },
]

export const router = createRouter({
  history: createWebHashHistory(import.meta.env.VITE_PUBLIC_PATH),
  routes,
  strict: true,
  scrollBehavior: () => ({ left: 0, top: 0 }),
})
