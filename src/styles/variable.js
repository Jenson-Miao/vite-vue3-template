import {useCssVar} from '@vueuse/core'
import variableColor from '@/config/color'
import {humpToMidline} from "@/utils/string";

const el = document.documentElement
Object.keys(variableColor).forEach(key => {
  const midlineKey = humpToMidline(key)
  const elColor = useCssVar(`--el-${  midlineKey}`, el)
  const color = useCssVar(`--${  midlineKey}`, el)
  elColor.value = variableColor[key]
  color.value = variableColor[key]
})

