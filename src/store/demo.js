import { defineStore } from 'pinia'

export default defineStore({
  id: 'demo',
  state: () => ({
    count: 0,
  }),
  getters: {
    getCount() {
      return `getter获取的数据：${this.count}`
    },
  },
  actions: {
    setCount(count) {
      this.count = count
    },
  },
})
